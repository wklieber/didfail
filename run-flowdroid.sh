#!/bin/bash
if [ $# -lt 2 ]; then
    echo "Usage: `basename $0` outdir apk"
    exit
fi
export outdir=$1
export apk_file=$2
export outdir=`readlink -m $outdir`

apk_base=`basename $apk_file`
apk_base=${apk_base%%.apk}
apk_xform=$outdir/$apk_base.apk

ulimit -v $max_mem -t $max_time

latest=
libaxmlver=1.0
if [ $newsoot -eq 1 ]; then
    #latest=-latest
    libaxmlver=2.0
fi

soot_classpath=$soot_paths:$(echo "$wkspc/soot-infoflow-android$latest/bin
$wkspc/soot-infoflow-android$latest/lib/polyglot.jar
$wkspc/soot-infoflow-android$latest/lib/AXMLPrinter2.jar
$wkspc/soot-infoflow$latest/bin
$wkspc/soot-infoflow$latest/lib/cos.jar
$wkspc/soot-infoflow$latest/lib/j2ee.jar
$wkspc/soot-infoflow$latest/lib/slf4j-api-1.7.5.jar
$wkspc/soot-infoflow$latest/lib/slf4j-simple-1.7.5.jar
$wkspc/soot-infoflow-android$latest/lib/axml-$libaxmlver.jar" | tr "\n" ":")

export flowdroid="java $jvm_flags -Dfile.encoding=UTF-8 -classpath $soot_classpath soot.jimple.infoflow.android.TestApps.Test"

echo Running FlowDroid on $apk_file
orig_wd=`pwd`
cd $wkspc/soot-infoflow-android$latest
$flowdroid $apk_xform $sdk_platforms --nostatic --aplength 1 --aliasflowins --out $outdir/$apk_base.fd.xml &> $outdir/log/$apk_base.flowdroid.log
err=$?; if [ $err -ne 0 ]; then echo "Failure!"; exit $err; fi
cd $orig_wd
